package initializer;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.ParallelBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import utils.CONSTANTS;
import vrp.Solution;
import jade.gui.GuiAgent;
import jade.gui.GuiEvent;


public class InitializerAgent extends GuiAgent {
	private InitializerGui gui;
	
    @Override
    protected void setup() {
	gui=new InitializerGui();
	gui.setInitializerAgent(this);
    System.out.println("Démarrage de l'agent Initializer");
    //ParallelBehaviour parallelBehaviour =new ParallelBehaviour();
   // addBehaviour(parallelBehaviour);
    //parallelBehaviour.addSubBehaviour(new OneShotBehaviour() {
		
	//	@Override
	//	public void action() {
			
	//		gui.showMessage("Behaviour OneShot", true);
			
	//	}  //la methode done retourne toujours true
		   //comportement
//	});
    
    //parallelBehaviour.addSubBehaviour(new TickerBehaviour(this,1000) {
	//	private int counter;
	//	@Override
	//	protected void onTick() {
	//		++counter;
		//	gui.showMessage("Ticker behaviour"+counter, true);
			//le taritement qui va s execute periodiquement
		//}
	//});
    //parallelBehaviour.addSubBehaviour
    addBehaviour(new CyclicBehaviour() {
		@Override
		public void action() {

			
			ACLMessage aclMessage=receive(); //messageTemplate
		     if(aclMessage!=null) {
		    	 
		    	 gui.showMessage("Sender:"+ aclMessage.getSender(), true);
		    	 gui.showMessage("acte de communication:"+ ACLMessage.getPerformative(aclMessage.getPerformative()),true);
		    	 gui.showMessage("Content:"+ aclMessage.getContent(), true);
		    	 gui.showMessage("Langue:"+ aclMessage.getLanguage(), true);
		    	 gui.showMessage("Ontology:"+ aclMessage.getOntology(), true);
		    	 gui.showMessage("X: "+ aclMessage.getUserDefinedParameter("x"), true);
		    	 gui.showMessage("Y: "+ aclMessage.getUserDefinedParameter("y"), true);
		     }
		     else block();
			
			
		}      // la methode done retourne toujours false
	});
    
  //parallelBehaviour.addSubBehaviour(new Behaviour() {
  	//	private int counter;
  		//@Override
  		//public boolean done() {
  		//	if(counter==8) return true;
  		//	else return false;
  	//	}
  		
  		//@Override
  		//public void action() {
  		//	++counter;
  	//		gui.showMessage("Generic Behaviour"+counter, true);
  			
  	//	}
  	//});
    }

    @Override
    protected void onGuiEvent(GuiEvent ev) {
	 switch (ev.getType()) {
	case 1:

		Map<String, Object> params=(Map<String, Object>) ev.getParameter(0);
		String problemID=(String)params.get("problemID");
		/**
		 * Start Calculate initial solution
		 */
			System.out.println("Start Calculate initial solution ");
			gui.showMessage("Start Calculate initial solution ", false);
			Solution initialSolution = calculateInitialSolution(problemID);
			System.out.println("End Calculate initial solution :"+initialSolution);
			gui.showMessage("End Calculate initial solution :"+initialSolution, false);
		/**
		 * End Calculate initial solution
		 */
		
		String agentAlgo=(String)params.get("algorithmAgentName");
		ACLMessage aclMessage=new ACLMessage(ACLMessage.REQUEST);
		aclMessage.addReceiver(new AID(agentAlgo, AID.ISLOCALNAME));
		try {
			aclMessage.setContentObject(initialSolution);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		aclMessage.setOntology("VRP-resolution");
		aclMessage.setConversationId(CONSTANTS.CONVERSATION_ID_FROM_INITIALIZER_TO_ALGORITHM);
		send(aclMessage);
		System.out.println("Initial Solution sent to algorithm agent :"+agentAlgo);
		gui.showMessage("Initial Solution sent to algorithm agent :"+agentAlgo, false);
		break;

	default:
		break;
	}
	
    }

	private Solution calculateInitialSolution(String problemID) {
		// TODO Auto-generated method stub
		Solution solution = new Solution();
		solution.setCurrentSituation("{{{ SOLUTION }}}");
		solution.setDelta(0);
		solution.setDeltaComparingLastOperation(0);
		solution.setHistory(new ArrayList<Solution>());
		return solution;
	}
}