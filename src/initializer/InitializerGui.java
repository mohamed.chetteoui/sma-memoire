package initializer;
// interface graphique
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import jade.gui.GuiEvent;
import utils.CONSTANTS;

import java.util.Map;

public class InitializerGui extends JFrame {
private JButton jButtonEnvoyer=new JButton("Process");
private JTextArea jTextAreaMess=new JTextArea();
private InitializerAgent initializerAgent;

public InitializerGui() {
	jTextAreaMess.setFont(new Font("Arial", Font.BOLD, 14)); //police et taille de texte 
	jTextAreaMess.setEditable(false); //la zone de texte ne doit pas etre modifiables si elle est false
	JPanel jPanelN=new JPanel();
	jPanelN.setLayout(new FlowLayout());
	jPanelN.add(jButtonEnvoyer);
	this.setLayout(new BorderLayout());
	this.add(jPanelN,BorderLayout.NORTH);
	this.add(new JScrollPane(jTextAreaMess), BorderLayout.CENTER);
	this.setSize(600, 400);
	this.setVisible(true);
	setTitle("Initial solution");
	jButtonEnvoyer.addActionListener(new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			String agentName= CONSTANTS.ALGORITHM_AGENT;
		    GuiEvent gev=new GuiEvent(this, 1);
			Map<String, Object> params=new HashMap<>();
	        params.put("algorithmAgentName", CONSTANTS.ALGORITHM_AGENT);
	        params.put("problemID", CONSTANTS.PROBLEM_1);
			
			gev.addParameter(params);
		    initializerAgent.onGuiEvent(gev);
		}
	});
}
public InitializerAgent getConsomateurAgent() {
	return initializerAgent;
}
public void setInitializerAgent(InitializerAgent initializerAgent) {
	this.initializerAgent = initializerAgent;
}
public void showMessage(String msg,boolean append) {
	if (append=true) {
		jTextAreaMess.append(msg+"\n");
	}
	else 
		jTextAreaMess.setText(msg);
}

}
