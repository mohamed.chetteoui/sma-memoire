package utils;

public class CONSTANTS {

	public static final String ALGORITHM_AGENT = "algorithm";
	public static final Object PROBLEM_1 = "problem1";
	public static final String CONVERSATION_ID_FROM_INITIALIZER_TO_ALGORITHM = "initializer_algorithm";
	public static final String CONVERSATION_ID_FROM_ALGORITHM_TO_DESTROYREPAIRAGENT = "algo_destroyrepair";
	public static final String CONVERSATION_ID_FROM_DESTROYREPAIRAGENT_TO_ALGORITHM = "destroyrepair_algo";
	public static final int K = 10000;
	public static final int DELTA_LIMIT_FOR_SMALL = 10000;
	public static final int ITERATIONS_LIMIT_FOR_SMALL = 5;

}
