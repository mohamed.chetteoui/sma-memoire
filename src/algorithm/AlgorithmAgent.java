package algorithm;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import destroyrepair.DestroyRepairType;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.ParallelBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.UnreadableException;
import utils.CONSTANTS;
import vrp.Solution;

public class AlgorithmAgent extends Agent {
	public List<AID> listSmallDestroyRepairs = new ArrayList<AID>();
	public List<AID> listLargeDestroyRepairs = new ArrayList<AID>();
	protected int iterations;
	protected Solution bestSolution;
	private List<Solution> returnedSolutions = new ArrayList<Solution>();
	private AlgorithmGui gui;
	protected Object currentOperatorsType;
	protected List<AID> currentOperators;
	protected int interationNumberSinceLastLarge;

	@Override
	protected void setup() {
		this.gui = new AlgorithmGui();
		ParallelBehaviour parallelBehaviour = new ParallelBehaviour();
		
		currentOperatorsType = DestroyRepairType.DS;
		
		addBehaviour(parallelBehaviour);
		parallelBehaviour.addSubBehaviour(new TickerBehaviour(this, 5000) {

			@Override
			protected void onTick() {
				DFAgentDescription template = new DFAgentDescription();
				ServiceDescription sd = new ServiceDescription();
				sd.setType("small-destroy-repair");
				template.addServices(sd);
				try {
					DFAgentDescription[] result = DFService.search(myAgent, template);
//					listDestroyRepairs = new ArrayList<>();

					for (DFAgentDescription agent : result) {
						if (!listSmallDestroyRepairs.contains(agent.getName())) {
							listSmallDestroyRepairs.add(agent.getName());
						}
					}
				} catch (FIPAException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				DFAgentDescription template2 = new DFAgentDescription();
				ServiceDescription sd2 = new ServiceDescription();
				sd.setType("large-destroy-repair");
				template.addServices(sd2);
				try {
					DFAgentDescription[] result = DFService.search(myAgent, template2);
//					listDestroyRepairs = new ArrayList<>();

					for (DFAgentDescription agent : result) {
						if (!listLargeDestroyRepairs.contains(agent.getName())) {
							listLargeDestroyRepairs.add(agent.getName());
						}
					}
				} catch (FIPAException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});
		parallelBehaviour.addSubBehaviour(new CyclicBehaviour() {

			@Override
			public void action() {
				ACLMessage aclMessage = receive();

				if (aclMessage != null) {
					String conversationID = aclMessage.getConversationId();
					switch (conversationID) {
					case CONSTANTS.CONVERSATION_ID_FROM_INITIALIZER_TO_ALGORITHM:
						Solution initialSolution;
						try {
							
							initialSolution = (Solution) aclMessage.getContentObject();
							bestSolution = initialSolution;
							
							System.out.println("Problem resolution request received : " + initialSolution);
							gui.showMessage("Problem resolution request received : " + initialSolution, true);
							
							retry(listSmallDestroyRepairs);

						} catch (UnreadableException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}

						break;
					case CONSTANTS.CONVERSATION_ID_FROM_DESTROYREPAIRAGENT_TO_ALGORITHM:
						System.out.println("candidate solution received from " + aclMessage.getSender().getName());
						gui.showMessage("candidate solution received from " + aclMessage.getSender().getName(), true);
						try {
							returnedSolutions.add((Solution) aclMessage.getContentObject());
							
							bestSolution = findBest();

							
							
							if(interationNumberSinceLastLarge == CONSTANTS.ITERATIONS_LIMIT_FOR_SMALL && bestSolution.getDeltaComparingLastLargeOperator() < CONSTANTS.DELTA_LIMIT_FOR_SMALL) {
								currentOperatorsType = DestroyRepairType.DL;
								gui.showMessage("The next operators are DS", true);
							} else {
								currentOperatorsType = DestroyRepairType.DS;
								gui.showMessage("The next operators are DL", true);
							}
							
							if (currentOperatorsType.equals(DestroyRepairType.DS)) {
								currentOperators = listSmallDestroyRepairs;
							} else {
								currentOperators = listLargeDestroyRepairs;
							}
							
							if (currentOperators.size() == returnedSolutions.size()) {
								
								
								System.out.println("All candidate solutions received");
								gui.showMessage("All candidate solutions received", true);
								
								
								
								
								if (iterations == CONSTANTS.K) {
									returnFinalResult(aclMessage);
								} else {
									retry(currentOperators);
									if(currentOperatorsType.equals(DestroyRepairType.DS)) {
										interationNumberSinceLastLarge ++;
									} else {
										interationNumberSinceLastLarge = 0;
									}
								}

								iterations++;
								returnedSolutions = new ArrayList<>();
							}
						} catch (UnreadableException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}

						break;
					default:
						break;
					}

					/*
					 * transaction
					 */

					/*
					 * fin transaction
					 */

//				send(aclMessage2);
				} else
					block();
			}

			

			
		});

	}
	
	private void retry(List<AID> currentOperators) {
		if (iterations != CONSTANTS.K) {
			for (AID aid : currentOperators) {

				ACLMessage aclMessage1 = new ACLMessage(ACLMessage.CFP);
				try {
					aclMessage1.setContentObject(bestSolution);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				aclMessage1.addReceiver(aid);
				aclMessage1.setConversationId(CONSTANTS.CONVERSATION_ID_FROM_ALGORITHM_TO_DESTROYREPAIRAGENT);
				send(aclMessage1);
				System.out.println("Problem sent to destroy repair agent: " + aid.getName());
				gui.showMessage("Problem sent to destroy repair agent: " + aid.getName(), true);
			}
		}

	}

	private Solution findBest() {
		System.out.println("Comparing candidate solution & Finding Best Solution");
		bestSolution = Collections.min(returnedSolutions, Comparator.comparing(s -> s.getCost()));
		gui.showMessage("Comparing candidate solution & Finding Best Solution", true);
		gui.showMessage("Best solution until now COST = " + bestSolution.getCost(), true);
		return bestSolution;
	}
	
	protected void returnFinalResult(ACLMessage aclMessage) {
		ACLMessage aclMessage2 = aclMessage.createReply();
		aclMessage2.setPerformative(ACLMessage.INFORM);
		try {
			aclMessage2.setContentObject(bestSolution);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		send(aclMessage2);
		System.out.println("FINAL SOLUTION FOUND");
		gui.showMessage("FINAL SOLUTION FOUND COST = " + bestSolution.getCost(), true);
		
	}

}
