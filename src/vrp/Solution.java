package vrp;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Solution implements Serializable {
	private int deltaComparingLastOperation;
	private int deltaComparingLastLargeOperator;
	private int delta;
	private List<Solution> history;
	private String currentSituation;
	private int cost;
	
	public Solution() {
		history = new ArrayList<Solution>();
	}
	
	public int getCost() {
		return cost;
	}
	public void setCost(int cost) {
		this.cost= cost;
	}
	public boolean isValid() {
		return true;
	}
	
	
	
	public int getDeltaComparingLastLargeOperator() {
		return deltaComparingLastLargeOperator;
	}

	public void setDeltaComparingLastLargeOperator(int deltaComparingLastLargeOperator) {
		this.deltaComparingLastLargeOperator = deltaComparingLastLargeOperator;
	}

	public int getDeltaComparingLastOperation() {
		return deltaComparingLastOperation;
	}
	public void setDeltaComparingLastOperation(int deltaComparingLastOperation) {
		this.deltaComparingLastOperation = deltaComparingLastOperation;
	}
	public int getDelta() {
		return delta;
	}
	public void setDelta(int delta) {
		this.delta = delta;
	}
	public List<Solution> getHistory() {
		return history;
	}
	public void setHistory(List<Solution> history) {
		this.history = history;
	}
	public String getCurrentSituation() {
		return currentSituation;
	}
	public void setCurrentSituation(String currentSituation) {
		this.currentSituation = currentSituation;
	}
	
	
}
