package vrp;

import java.io.Serializable;

public class Customer extends HasPositions implements Serializable {

	private int demand;
	private Satellite affectedSatellite;

	public int getDemand() {
		return demand;
	}

	public void setDemand(int demand) {
		this.demand = demand;
	}

	@Override
	public double calculateDistance(HasPositions another) {
		// TODO Auto-generated method stub
		return super.calculateDistance(another) * 100 * 2;
	}

	public Satellite getAffectedSatellite() {
		return affectedSatellite;
	}

	public void setAffectedSatellite(Satellite affectedSatellite) {
		this.affectedSatellite = affectedSatellite;
	}

}
