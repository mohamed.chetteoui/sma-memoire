package vrp;

import java.io.Serializable;

public class Satellite extends HasPositions implements Serializable {

	private int capacity;
	private int coutOuverture;
	private boolean open;

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public int getCoutOuverture() {
		return coutOuverture;
	}

	public void setCoutOuverture(int coutOuverture) {
		this.coutOuverture = coutOuverture;
	}

	@Override
	public double calculateDistance(HasPositions another) {
		// TODO Auto-generated method stub
		return super.calculateDistance(another) * 100;
	}

	public boolean isOpen() {
		return open;
	}

	public void setOpen(boolean open) {
		this.open = open;
	}

	

}
