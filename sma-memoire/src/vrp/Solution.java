package vrp;

import java.awt.Color;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

public class Solution implements Serializable {
	private double deltaComparingLastOperation;
	private double deltaComparingLastLargeOperator;
	private double delta;
	private String currentSituation;

	private Depot depot;
	private List<Vehicule> vehicules;
	private List<Satellite> satellites;
	private List<Customer> customers;

	public Depot getDepot() {
		return depot;
	}

	public void setDepot(Depot depot) {
		this.depot = depot;
	}

	public List<Vehicule> getVehicules() {
		return vehicules;
	}

	public void setVehicules(List<Vehicule> vehicules) {
		this.vehicules = vehicules;
	}

	public List<Satellite> getSatellites() {
		return satellites;
	}

	public void setSatellites(List<Satellite> satellites) {
		this.satellites = satellites;
	}

	public List<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(List<Customer> customers) {
		this.customers = customers;
	}

	public Solution() {
		customers = new ArrayList();
		satellites = new ArrayList();
		vehicules = new ArrayList();
	}



	public boolean isValid() {
		return true;
	}

	public double getDeltaComparingLastLargeOperator() {
		return deltaComparingLastLargeOperator;
	}

	public void setDeltaComparingLastLargeOperator(double deltaComparingLastLargeOperator) {
		this.deltaComparingLastLargeOperator = deltaComparingLastLargeOperator;
	}

	public double getDeltaComparingLastOperation() {
		return deltaComparingLastOperation;
	}

	public void setDeltaComparingLastOperation(double deltaComparingLastOperation) {
		this.deltaComparingLastOperation = deltaComparingLastOperation;
	}

	public double getDelta() {
		return delta;
	}

	public void setDelta(double delta) {
		this.delta = delta;
	}

	

	public String getCurrentSituation() {
		return currentSituation;
	}

	public void setCurrentSituation(String currentSituation) {
		this.currentSituation = currentSituation;
	}

	public void draw(ChartPanel chartPanel, String string) {
		// Create dataset
		XYDataset dataset = createDataset();

		// Create chart
		JFreeChart chart = ChartFactory.createScatterPlot("LRP-2E problem : " + string + " Cost " + calculateCost(),
				"X-Axis", "Y-Axis", dataset);

		// Changes background color
		XYPlot plot = (XYPlot) chart.getPlot();
		plot.setBackgroundPaint(new Color(255, 228, 196));

		XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) plot.getRenderer();
		renderer.setBaseShapesVisible(true);
		chart.getXYPlot().getRenderer().setSeriesVisibleInLegend(2, false);
		for (int i = 0; i < plot.getSeriesCount(); i++) {
			if (i < 3) {
				renderer.setSeriesLinesVisible(i, false);
				renderer.setSeriesShapesVisible(i, true);
			} else {
				renderer.setSeriesLinesVisible(i, true);
				renderer.setSeriesShapesVisible(i, false);
			}
		}

		// Create Panel
		chartPanel.setChart(chart);
		chartPanel.revalidate();
		chartPanel.repaint();
	}

	private XYDataset createDataset() {

		XYSeriesCollection dataset = new XYSeriesCollection();

		XYSeries series0 = new XYSeries("Clients", false, true);

		for (Customer customer : customers) {
			series0.add(customer.getX(), customer.getY());

		}

		dataset.addSeries(series0);

		XYSeries series1 = new XYSeries("Satellites");

		for (Satellite satellite : satellites) {
			series1.add(satellite.getX(), satellite.getY());

		}
		dataset.addSeries(series1);

		XYSeries seriesDepot = new XYSeries("FAKE");
		seriesDepot.add(depot.getX(), depot.getY());

		dataset.addSeries(seriesDepot);

		XYSeries fake = new XYSeries("Depots");
		fake.add(-1, -1);
		dataset.addSeries(fake);

		for (Vehicule vehicule : vehicules) {
			XYSeries routes = new XYSeries("Route from " + vehicule.getRoute().get(0).getX() + ","
					+ vehicule.getRoute().get(0).getY() + " " + UUID.randomUUID(), false, true);
			for (HasPositions hasPositions : vehicule.getRoute()) {
				if (hasPositions instanceof Satellite && !((Satellite) hasPositions).isOpen()) {
					continue;
				} else {
					routes.add(hasPositions.getX(), hasPositions.getY());
				}
			}
			dataset.addSeries(routes);
		}

		return dataset;
	}

	public void calculateInitialSolution() {

		for (Customer customer : customers) {
			Satellite nearestSatellite = satellites.get(0);
			double minimumDistance = customer.calculateDistance(nearestSatellite);
			for (Satellite satellite : satellites) {
				double distance = customer.calculateDistance(satellite);
				if (distance < minimumDistance) {
					nearestSatellite = satellite;
					minimumDistance = distance;
				}
			}
			customer.setAffectedSatellite(nearestSatellite);

		}
		for (Satellite satellite : satellites) {
			Vehicule vehicule = new Vehicule(2);

			satellite.setOpen(true);
			for (Customer customer : customers) {
				if (customer.getAffectedSatellite().equals(satellite)) {
					vehicule.getRoute().add(customer);
				}
			}
			vehicule.getRoute().sort(new Comparator<HasPositions>() {

				@Override
				public int compare(HasPositions o1, HasPositions o2) {

					return o1.getX() - o2.getX();
				}
			});
			vehicule.getRoute().add(0, satellite);
			vehicule.getRoute().add(satellite);
			vehicules.add(vehicule);
		}

		for (Satellite satellite : satellites) {
			Vehicule vehicule = new Vehicule(1);
			if (!satellite.isOpen())
				continue;
			vehicule.getRoute().add(depot);

			vehicule.getRoute().add(satellite);

			vehicules.add(vehicule);
		}

	}

	public double calculateCost() {
		double cost = 0;
		for (Vehicule vehicule : vehicules) {
			cost += vehicule.getCost();
			for (int i = 1; i < vehicule.getRoute().size(); i++) {
				if (vehicule.getRoute().size() > 1)
					cost += vehicule.getRoute().get(i).calculateDistance(vehicule.getRoute().get(i - 1));
			}
		}
		for (Satellite satellite : satellites) {
			cost += satellite.getCoutOuverture();
		}
		return cost;
	}
}
