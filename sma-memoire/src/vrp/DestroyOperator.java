package vrp;

@FunctionalInterface
public interface DestroyOperator {

	public Solution destroy(Solution solution);
}
