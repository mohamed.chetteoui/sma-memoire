package vrp;

public class DestroyRepairOperator {
	private DestroyOperator destroyOperator;
	private RepairOperator repairOperator;

	public DestroyRepairOperator(DestroyOperator destroyOperator, RepairOperator repairOperator) {
		super();
		this.destroyOperator = destroyOperator;
		this.repairOperator = repairOperator;
	}

	public DestroyOperator getDestroyOperator() {
		return destroyOperator;
	}

	public void setDestroyOperator(DestroyOperator destroyOperator) {
		this.destroyOperator = destroyOperator;
	}

	public RepairOperator getRepairOperator() {
		return repairOperator;
	}

	public void setRepairOperator(RepairOperator repairOperator) {
		this.repairOperator = repairOperator;
	}

}
