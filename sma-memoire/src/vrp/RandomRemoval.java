package vrp;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RandomRemoval implements DestroyOperator {

	@Override
	public Solution destroy(Solution solution) {
		// TODO Auto-generated method stub
//		for (Vehicule vehicule : solution.getVehicules()) {
//			vehicule.getRoute().removeAll(getNElements(solution.getCustomers(), 3));
//		}
		return solution;
	}

	public static List<Customer> getNElements(List<Customer> list, Integer n) {
		List<Customer> rtn = null;
		Random rand = new Random();

		if (list != null && n != null && n > 0) {
			int lSize = list.size();
			if (lSize > n) {
				rtn = new ArrayList<Customer>(n);
				Customer[] es = (Customer[]) list.toArray();
				// Knuth-Fisher-Yates shuffle algorithm
				for (int i = es.length - 1; i > es.length - n - 1; i--) {
					int iRand = rand.nextInt(i + 1);
					Customer eRand = es[iRand];
					es[iRand] = es[i];
					// This is not necessary here as we do not really need the final shuffle result.
					// es[i] = eRand;
					rtn.add(eRand);
				}

			} else if (lSize == n) {
				rtn = new ArrayList<Customer>(n);
				rtn.addAll(list);
			} else {
				System.out.println("list.size < nSub! " + lSize + " " + n);
			}
		}

		return rtn;
	}
}
