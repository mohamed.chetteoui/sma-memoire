package vrp;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import jade.util.leap.LinkedList;

public class Vehicule implements java.io.Serializable{

	private int level;
	private int cost;
	private int capacity;
	private List<HasPositions> route;

	public Vehicule(int level) {
		super();
		this.level = level;
		if (level == 1) {
			cost = utils.FileReader.FIXED_CAPACITY_VEHICULE_FIRST_LEVEL;
			capacity = utils.FileReader.FIXED_COST_VEHICULE_FIRST_LEVEL;
		}
		if (level == 2) {
			cost = utils.FileReader.FIXED_CAPACITY_VEHICULE_SECOND_LEVEL;
			capacity = utils.FileReader.FIXED_COST_VEHICULE_SECOND_LEVEL;
		}
		route = new ArrayList<HasPositions>();
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public List<HasPositions> getRoute() {
		return route;
	}

	public void setRoute(List<HasPositions> route) {
		this.route = route;
	}
}
