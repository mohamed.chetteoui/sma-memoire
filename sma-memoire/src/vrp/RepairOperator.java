package vrp;

@FunctionalInterface
public interface RepairOperator {

	public Solution repairSolution(Solution solution);
}
