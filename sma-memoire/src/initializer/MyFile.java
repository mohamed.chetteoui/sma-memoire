package initializer;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MyFile extends File{

	public MyFile(String pathname) {
		super(pathname);
		// TODO Auto-generated constructor stub
	}
	
	public List<MyFile> listMyFiles() {
		// TODO Auto-generated method stub
		List<MyFile> myFiles = new ArrayList();
		
		List<File> files = Arrays.asList(super.listFiles());
		for(File file: files) {
			myFiles.add(new MyFile(file.getAbsolutePath()));
		}
		return myFiles;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.getName();
	}
	
}
