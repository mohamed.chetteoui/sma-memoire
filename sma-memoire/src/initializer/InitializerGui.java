package initializer;

// interface graphique
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;

import jade.gui.GuiEvent;
import utils.CONSTANTS;
import utils.FileReader;
import vrp.Solution;

import java.util.Map;

public class InitializerGui extends JFrame {
	static JFreeChart chart = null;
	private JButton jButtonEnvoyer = new JButton("Process");
	private JTextArea jTextAreaMess = new JTextArea();
	private InitializerAgent initializerAgent;
	private JList displayList;
	private ChartPanel chartPanel;

	public InitializerGui() {
		jTextAreaMess.setFont(new Font("Arial", Font.BOLD, 14)); // police et taille de texte
		jTextAreaMess.setEditable(false); // la zone de texte ne doit pas etre modifiables si elle est false
		JPanel jPanelN = new JPanel();
		jPanelN.setLayout(new FlowLayout());
		jPanelN.add(jButtonEnvoyer);
		this.setLayout(new BorderLayout());
		this.add(jPanelN, BorderLayout.NORTH);
		this.add(new JScrollPane(jTextAreaMess), BorderLayout.CENTER);
		this.setSize(600, 400);
		this.setVisible(true);
		setTitle("Initial solution");
		initFiles();
		jButtonEnvoyer.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String agentName = CONSTANTS.ALGORITHM_AGENT;
				GuiEvent gev = new GuiEvent(this, 1);
				Map<String, Object> params = new HashMap<>();
				params.put("algorithmAgentName", CONSTANTS.ALGORITHM_AGENT);
				params.put("problemID", CONSTANTS.PROBLEM_1);

				gev.addParameter(params);
				MyFile myFile = (MyFile) displayList.getSelectedValue();
				Solution solution = FileReader.read(myFile.getAbsolutePath());
				solution.calculateInitialSolution();
				solution.draw(chartPanel, myFile.toString());
				initializerAgent.setCurrentProblem(solution);
				initializerAgent.onGuiEvent(gev);
			}
		});
	}

	public InitializerAgent getConsomateurAgent() {
		return initializerAgent;
	}

	public void setInitializerAgent(InitializerAgent initializerAgent) {
		this.initializerAgent = initializerAgent;
	}

	public void showMessage(String msg, boolean append) {
		if (append = true) {
			jTextAreaMess.append(msg + "\n");
		} else
			jTextAreaMess.setText(msg);
	}

	public void initFiles() {
		String PATH = "C:\\Users\\moham\\OneDrive\\Bureau\\" + "Prodhon's LRP-2E instances\\";
		MyFile[] files = new MyFile(PATH).listMyFiles().stream().toArray(MyFile[]::new);

		displayList = new JList(files);
		displayList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
		displayList.setLayoutOrientation(javax.swing.JList.HORIZONTAL_WRAP);
		displayList.setName("displayList");
		InitializerGui f = this;
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setPreferredSize(new Dimension(500, 300));

		chartPanel = new ChartPanel(chart);

		displayList.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

			@Override
			public void valueChanged(ListSelectionEvent e) {
				// TODO Auto-generated method stub
				if (e.getValueIsAdjusting())
					System.out.println(files[e.getFirstIndex()]);
				Solution solution = FileReader.read(files[e.getFirstIndex()].getAbsolutePath());
//				solution.calculateInitialSolution();
				solution.draw(chartPanel, files[e.getFirstIndex()].toString());
			}
		});
		displayList.setVisibleRowCount(-1);
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
		mainPanel.add(new JScrollPane(displayList));
		mainPanel.add(chartPanel);
		f.add(new JScrollPane(mainPanel),BorderLayout.SOUTH);
		f.pack();
		f.setVisible(true);
	}

	public void calculateInitialSolution() {

	}

}
