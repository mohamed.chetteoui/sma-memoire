package utils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import vrp.Customer;
import vrp.Depot;
import vrp.Satellite;
import vrp.Solution;

public class FileReader {

	public static int FIXED_COST_VEHICULE_FIRST_LEVEL;
	public static int FIXED_COST_VEHICULE_SECOND_LEVEL;
	public static int FIXED_CAPACITY_VEHICULE_FIRST_LEVEL;
	public static int FIXED_CAPACITY_VEHICULE_SECOND_LEVEL;

	public static Solution read(String fileName) {
		List<String> lines = null;
		try (Stream<String> linesX = Files.lines(Paths.get(fileName))) {
			lines = linesX.collect(Collectors.toList());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Solution solution = new Solution();
		lines.forEach(System.out::println);

		Depot depot = new Depot();
		int depotX = Integer.parseInt(lines.get(3).split("\t")[0]);
		depot.setX(depotX);
		int depotY = Integer.parseInt(lines.get(3).split("\t")[1]);
		depot.setY(depotY);
		solution.setDepot(depot);

		int numberOfCustomers = Integer.parseInt(lines.get(0));
		int numberOfSatellites = Integer.parseInt(lines.get(1));

		for (int i = 4; i < 4 + numberOfSatellites; i++) {
			Satellite customer = new Satellite();
			customer.setX(Integer.parseInt(lines.get(i).split("\t")[0]));
			customer.setY(Integer.parseInt(lines.get(i).split("\t")[1]));
			solution.getSatellites().add(customer);
		}
		for (int i = 5 + numberOfSatellites; i < 5 + numberOfSatellites + numberOfCustomers; i++) {
			Customer customer = new Customer();
			customer.setX(Integer.parseInt(lines.get(i).split("\t")[0]));
			customer.setY(Integer.parseInt(lines.get(i).split("\t")[1]));
			solution.getCustomers().add(customer);
		}

		int index = 5 + numberOfSatellites + numberOfCustomers;

		FIXED_COST_VEHICULE_FIRST_LEVEL = Integer.parseInt(lines.get(index + 1));
		FIXED_COST_VEHICULE_SECOND_LEVEL = Integer.parseInt(lines.get(index + 2));

		index = index + 4;

		int satelliteIterator = 0;
		for (int i = index; i < index + numberOfSatellites; i++) {
			solution.getSatellites().get(satelliteIterator).setCapacity(Integer.parseInt(lines.get(i)));
			satelliteIterator++;

		}
		index = index + numberOfSatellites + 1;

		int customerIterator = 0;
		for (int i = index; i < index + numberOfCustomers; i++) {
			solution.getCustomers().get(customerIterator).setDemand(Integer.parseInt(lines.get(i)));
			customerIterator++;
		}

		index = index + numberOfCustomers + 1;

		satelliteIterator = 0;
		for (int i = index; i < index + numberOfSatellites; i++) {
			solution.getSatellites().get(satelliteIterator).setCoutOuverture(Integer.parseInt(lines.get(i)));
			satelliteIterator++;
		}

		index = index + numberOfSatellites + 1;

		FIXED_CAPACITY_VEHICULE_FIRST_LEVEL = Integer.parseInt(lines.get(index));
		FIXED_CAPACITY_VEHICULE_SECOND_LEVEL = Integer.parseInt(lines.get(index + 1));

		return solution;
	}

	public static void main(String[] args) {
		read("C:\\Users\\moham\\OneDrive\\Bureau\\Prodhon's LRP-2E instances\\coord20-5-1-2e.dat");
	}
}
