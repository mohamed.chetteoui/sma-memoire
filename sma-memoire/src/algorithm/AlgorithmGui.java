package algorithm;
// interface graphique
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import jade.gui.GuiEvent;
import utils.CONSTANTS;

import java.util.Map;

public class AlgorithmGui extends JFrame {
private JTextArea jTextAreaMess=new JTextArea();
private AlgorithmAgent algorithmAgent;

public AlgorithmGui() {
	jTextAreaMess.setFont(new Font("Arial", Font.BOLD, 14)); //police et taille de texte 
	jTextAreaMess.setEditable(false); //la zone de texte ne doit pas etre modifiables si elle est false
	JPanel jPanelN=new JPanel();
	jPanelN.setLayout(new FlowLayout());
	this.setLayout(new BorderLayout());
	this.add(jPanelN,BorderLayout.NORTH);
	this.add(new JScrollPane(jTextAreaMess), BorderLayout.CENTER);
	this.setSize(600, 400);
	this.setVisible(true);
	setTitle("Algorithm manager");
	
}





public AlgorithmAgent getAlgorithmAgent() {
	return algorithmAgent;
}





public void setAlgorithmAgent(AlgorithmAgent algorithmAgent) {
	this.algorithmAgent = algorithmAgent;
}





public void showMessage(String msg,boolean append) {
	if (append=true) {
		jTextAreaMess.append(msg+"\n");
	}
	else 
		jTextAreaMess.setText(msg);
}

}
