package destroyrepair;

import java.util.List;

import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;
import vrp.DestroyRepairOperator;
import vrp.GreedyInsertion;
import vrp.RandomRemoval;
//import jade.wrapper.ControllerException;

public class DestroyRepairContainer {
	public static void main(String[] args) {
		try {
			Runtime runtime = Runtime.instance();
			ProfileImpl profileImpl = new ProfileImpl(false);
			profileImpl.setParameter(ProfileImpl.MAIN_HOST, "localhost");
			AgentContainer agentContainer = runtime.createAgentContainer(profileImpl);
			DestroyRepairOperator destroyRepairOperator = new DestroyRepairOperator(new RandomRemoval(), new GreedyInsertion());
			
			for (int i = 0; i < 3; i++) {
				AgentController agentController = agentContainer.createNewAgent("small-destroy-repair-"+i,
						DestroyRepairAgent.class.getName(), new Object[] {destroyRepairOperator});
				agentController.start();
			}
			
			for (int i = 0; i < 1; i++) {
				AgentController agentController = agentContainer.createNewAgent("large-destroy-repair-"+i,
						DestroyRepairAgent.class.getName(), new Object[] {destroyRepairOperator});
				agentController.start();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}