package destroyrepair;

import java.io.IOException;
import java.util.Random;

import initializer.InitializerGui;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.ParallelBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.gui.GuiAgent;
import jade.gui.GuiEvent;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.UnreadableException;
import utils.CONSTANTS;
import vrp.DestroyRepairOperator;
import vrp.Solution;

public class DestroyRepairAgent extends GuiAgent {
	private DestroyRepairGui gui;
	private DestroyRepairType type;
	private DestroyRepairOperator operators;
	
	public DestroyRepairAgent() {
		// TODO Auto-generated constructor stub
	}
	

	@Override
	protected void setup() {
		this.operators = (DestroyRepairOperator) getArguments()[0];
		gui = new DestroyRepairGui();
		gui.setDestroyRepairAgent(this);
		type = getName().startsWith("small-") ? DestroyRepairType.DS : DestroyRepairType.DL;
		gui.setTitle(getName() + " --- " + type);
		ParallelBehaviour parallelBehaviour = new ParallelBehaviour();
		addBehaviour(parallelBehaviour);
		parallelBehaviour.addSubBehaviour(new OneShotBehaviour() {

			@Override
			public void action() {
				// TODO Auto-generated method stub
				DFAgentDescription dfAgentDescription = new DFAgentDescription();
				dfAgentDescription.setName(getAID());
				ServiceDescription serviceDescription = new ServiceDescription();
				if (type.equals(DestroyRepairType.DS)) {
					serviceDescription.setType("small-destroy-repair");
				} else {
					serviceDescription.setType("large-destroy-repair");
				}

				serviceDescription.setName("1");
				dfAgentDescription.addServices(serviceDescription);
				try {
					DFService.register(myAgent, dfAgentDescription);
				} catch (FIPAException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});

		parallelBehaviour.addSubBehaviour(new CyclicBehaviour() {

			@Override
			public void action() {
				ACLMessage aclMessage = receive();
				if (aclMessage != null) {
					switch (aclMessage.getPerformative()) {
					case ACLMessage.CFP:
						Solution oldSolution;
						try {
							oldSolution = (Solution) aclMessage.getContentObject();
							double costBeforeOperator = oldSolution.calculateCost();
							gui.showMessage("Problem received COST = " + oldSolution.calculateCost(), true);
							gui.showMessage(oldSolution.getCurrentSituation(), true);
							gui.showMessage("Applying destroy operator", true);
							Solution solution = operators.getDestroyOperator().destroy(oldSolution);
							gui.showMessage("Applying repair operator", true);
							solution = operators.getRepairOperator().repairSolution(oldSolution);
							gui.showMessage(oldSolution.getCurrentSituation(), true);
							ACLMessage reply = aclMessage.createReply();
							double newCost = solution.calculateCost();

							solution.setDeltaComparingLastOperation(newCost - costBeforeOperator);
							if(type.equals(DestroyRepairType.DS)) {
								solution.setDeltaComparingLastLargeOperator(solution.getDeltaComparingLastLargeOperator() + newCost - costBeforeOperator);
							} else {
								solution.setDelta(0);
							}
							solution.setDelta(solution.getDelta() + newCost - costBeforeOperator);
						
							reply.setContentObject(solution);
							reply.setConversationId(CONSTANTS.CONVERSATION_ID_FROM_DESTROYREPAIRAGENT_TO_ALGORITHM);
							send(reply);
							gui.showMessage("Solution returned to algorithm agent COST = " + solution.calculateCost(), true);
							gui.showMessage("************ Done ************", true);
						} catch (UnreadableException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						break;
					case ACLMessage.ACCEPT_PROPOSAL:

						break;
					default:
						break;
					}
				} else {
					block();
				}

			}

		});

	}

	@Override
	protected void onGuiEvent(GuiEvent arg0) {
		// TODO Auto-generated method stub

	}

	private int generateRandomCost(Solution solution) {
		Random random = new Random();
		int randomCost = random.nextInt();
		if (randomCost < 1000) {
			return generateRandomCost(solution);
		}
		return randomCost;

	}


	@Override
	protected void takeDown() {
		// TODO Auto-generated method stub
		try {
			DFService.deregister(this);
		} catch (FIPAException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void setType(DestroyRepairType ds) {
		this.type = ds;
	}

}
