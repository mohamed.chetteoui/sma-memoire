package destroyrepair;
// interface graphique
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import jade.gui.GuiEvent;
import java.util.Map;

public class DestroyRepairGui extends JFrame {
/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
private JTextArea jTextAreaMess=new JTextArea();
private DestroyRepairAgent destroyRepairAgent;

public DestroyRepairGui() {
	jTextAreaMess.setFont(new Font("Arial", Font.BOLD, 14)); //police et taille de texte 
	jTextAreaMess.setEditable(false); //la zone de texte ne doit pas etre modifiables si elle est false
	JPanel jPanelN=new JPanel();
	jPanelN.setLayout(new FlowLayout());
	this.setLayout(new BorderLayout());
	this.add(jPanelN,BorderLayout.NORTH);
	this.add(new JScrollPane(jTextAreaMess), BorderLayout.CENTER);
	this.setSize(600, 400);
	this.setVisible(true);
	
}



public DestroyRepairAgent getDestroyRepairAgent() {
	return destroyRepairAgent;
}



public void setDestroyRepairAgent(DestroyRepairAgent destroyRepairAgent) {
	this.setTitle(destroyRepairAgent.getName());
	this.destroyRepairAgent = destroyRepairAgent;
}



public void showMessage(String msg,boolean append) {
	if (append=true) {
		jTextAreaMess.append(msg+"\n");
	}
	else 
		jTextAreaMess.setText(msg);
}

}
